package com.foreverlink.wordChain;


import java.io.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Jacek Szmyd on 31.08.2017.
 */
public class WordChain {


    int length;
    private int bestDepth = -1;
    private List<Node<String>> goodNodes = null;
    private Set<String> dictionary = new HashSet<>();

    public static boolean diff1(String start, String end) {

        char[] s = start.toCharArray();
        char[] e = end.toCharArray();
        int d = 0;
        for (int i = 0; i < s.length; i++) {
            if (s[i] != e[i])
                d++;
        }
        if (d == 1)
            return true;
        return false;
    }

    public List<String> getShortestWordChain(String filename, String startWord, String endWord) throws Exception {
        return getShortestWordChain(loadDictionary(filename, startWord.length()), startWord, endWord);
    }

    public List<String> getShortestWordChain(Set<String> dictionary, String startWord, String endWord) throws Exception {
        this.bestDepth = -1;
        this.goodNodes = new LinkedList<>();
        this.dictionary = dictionary;
        if (startWord == null || startWord.isEmpty() || endWord == null || endWord.isEmpty() || startWord.length() != endWord.length())
            throw new Exception("Bad input!");

        if (!dictionary.contains(startWord) || !dictionary.contains(endWord)) {
            throw new Exception("Start word and or Word not in dictionary");
        }


        length = startWord.length();

        Node<String> root = new Node<>(startWord);
        recuur(endWord, root);
        // print result in reverse order
        // System.out.println("Found solutions: "+goodNodes.size());
        int count = -1;
        Node<String> bestNode = null;
        for (Node<String> n : goodNodes) {
            if (count == -1 || count > n.getDepth()) {
                count = n.getDepth();
                bestNode = n;
            }
        }
        // print best node;
        List<String> l = new LinkedList<>();
        if (bestNode != null) {
            Node<String> n = bestNode;
            do {
                l.add(0, n.getData());
                System.out.print(n.getData() + "\n");
                n = n.getParent();
            } while (n != null);
        }
        return l;
    }

    private void recuur(String end, Node<String> node) {
        if (bestDepth > 0 && node.getDepth() >= bestDepth)
            return;
        if (diff1(node.getData(), end)) {
            //System.out.println("Found chain: level:"+node.getDepth()+1);
            Node lastNode = node.addChild(end);
            if (bestDepth < 0) {
                bestDepth = lastNode.getDepth();
                goodNodes.add(lastNode);
            } else {
                if (lastNode.getDepth() <= bestDepth) {
                    bestDepth = lastNode.getDepth();
                    goodNodes.add(lastNode);
                }
            }
            return;
        }

        for (String s : dictionary) {
            if (checkIfInTree(s, node))
                continue;
            if (diff1(s, node.getData())) {
                recuur(end, node.addChild(s));
            }
        }
    }

    private boolean checkIfInTree(String word, Node<String> node) {
        do {
            if (node.getData().equals(word))
                return true;
            node = node.getParent();
        } while (node != null);
        return false;
    }

    public Set<String> loadDictionary(final String file, final int length) throws IOException {

        Set<String> dictionary = new HashSet<>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(new File(file)));
            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                if (currentLine.length() == length) {
                    dictionary.add(currentLine);
                }
            }
            return dictionary;

        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

