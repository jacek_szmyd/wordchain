package com.foreverlink.wordChain;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jacek Szmyd on 31.08.2017.
 */


public class Node<T> {
    int depth;
    private T data;
    private Node<T> parent;
    private List<Node<T>> children;

    public Node(T s) {
        this.data = s;
    }

    public int getDepth() {
        return depth;
    }

    private void setDepth(int depth) {
        this.depth = depth;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Node<T> getParent() {
        return parent;
    }

    public void setParent(Node<T> parent) {
        this.parent = parent;
    }

    public List<Node<T>> getChildren() {
        return children;
    }

    public Node<T> addChild(T val) {
        if (children == null)
            children = new LinkedList<>();
        Node<T> n = new Node<>(val);
        children.add(n);
        n.setDepth(depth + 1);
        n.setParent(this);
        return n;

    }
}

