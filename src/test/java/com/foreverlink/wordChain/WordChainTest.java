package com.foreverlink.wordChain;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Jacek Szmyd on 31.08.2017.
 */
public class WordChainTest {

    WordChain wordChain;

    @Before
    public void setUp() throws Exception {
        wordChain = new WordChain();

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void diff1Test() {
        Assert.assertTrue(WordChain.diff1("cat", "cot"));
    }

    @Test
    public void diff1WrongTest1() {
        Assert.assertFalse(WordChain.diff1("cat", "cat"));
    }

    @Test
    public void diff1WrongTest2() {
        Assert.assertFalse(WordChain.diff1("cat", "dog"));
    }

    @Test
    public void getShortestWordTest() throws Exception {
        String[] dict = {"cat", "zsd", "glo", "cot", "cog", "dog", "sad", "asd"};
        Set<String> dictionary = new HashSet<>();
        for (String s : dict) {
            dictionary.add(s);
        }
        List<String> result = wordChain.getShortestWordChain(dictionary, "cat", "dog");

        System.out.println("\n test results");
        for (String s : result) {
            System.out.println(s);
        }
        Assert.assertTrue(result.size() == 4);

    }

    @Test
    public void getShortestWordTestWrong() {
        String[] dict = {"cat", "zsd", "glo", "cot", "cog", "dog", "sad", "asd"};
        Set<String> dictionary = new HashSet<>();
        for (String s : dict) {
            dictionary.add(s);
        }

        try {
            wordChain.getShortestWordChain(dictionary, "zat", "dog");
            wordChain.getShortestWordChain(dictionary, "cat", "doa");
            Assert.assertTrue(false);
        } catch (Exception e) {

        }

    }

    @Test
    public void getShortestWordTestNotFound() throws Exception {
        String[] dict = {"aaa", "cat", "zsd", "glo", "cot", "cog", "dog", "sad", "asd"};
        Set<String> dictionary = new HashSet<>();
        for (String s : dict) {
            dictionary.add(s);
        }


        List<String> list = wordChain.getShortestWordChain(dictionary, "aaa", "dog");
        Assert.assertTrue(list.isEmpty());


    }

    @Test
    public void getShortestWordTest1() throws Exception {

        List<String> result = wordChain.getShortestWordChain("src/test/resources/wordlist.txt", "cat", "dog");

        System.out.println("\n test results");
        for (String s : result) {
            System.out.println(s);
        }
        Assert.assertTrue(result.size() == 4);

    }

}